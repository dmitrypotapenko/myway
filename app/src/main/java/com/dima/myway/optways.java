package com.dima.myway;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class Optways extends AppCompatActivity  {
    //private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    String start, finish, transp, put;
    TextView ways;
    //GoogleMap mMap;
    private DatabaseHelper databaseHelper;
    private Cursor cursor;
    private String table;
    //private MapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optways);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        Bundle mapViewBundle = null;
//        if (savedInstanceState != null) {
//            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
//        }
//        mMapView = (MapView) findViewById(R.id.mapView);
//        mMapView.onCreate(mapViewBundle);
//        mMapView.getMapAsync(this);

        databaseHelper = new DatabaseHelper(this);
        try {
            databaseHelper.checkAndCopyDatabase();
            databaseHelper.openDatabase();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

        Intent intent1 = getIntent();
        start = intent1.getStringExtra("start");
        finish = intent1.getStringExtra("finish");
        transp = intent1.getStringExtra("transp");
        put = intent1.getStringExtra("put");
        table = "optways";
        ways = (TextView) findViewById(R.id.ways);

        if (transp == "Троллейбус" && put == "Дешевый") {
            try {
                cursor = databaseHelper.QueryData("select * from " + table + " where " + "Start " + "= \'" + start + "\' AND " + " Finish " + "= \'" + finish + "\' AND " + " Transp " + "= \'" + transp + "\' AND " + " Put " + "= \'" + put + "\'");
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            ways.setText("Название маршрута " + (cursor.getString(3).toString())
                                    + "\n\n" + "Расстояние " + (cursor.getString(4)).toString()
                                    + "\n\n" + "Время " + (cursor.getString(5)).toString()
                                    + "\n\n" + "Цена " + (cursor.getString(6)).toString());

                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        } else if (transp == "Маршрутка" && put == "Быстрый") {
            try {
                cursor = databaseHelper.QueryData("select * from " + table + " where " + "Start " + "= \'" + start + "\' AND " + " Finish " + "= \'" + finish + "\' AND " + " Transp " + "= \'" + transp + "\' AND " + " Put " + "= \'" + put + "\'");
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            ways.setText("Название маршрута " + (cursor.getString(3).toString())
                                    + "\n\n" + "Расстояние " + (cursor.getString(4)).toString()
                                    + "\n\n" + "Время " + (cursor.getString(5)).toString()
                                    + "\n\n" + "Цена " + (cursor.getString(6)).toString());

                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//
//        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
//        if (mapViewBundle == null) {
//            mapViewBundle = new Bundle();
//            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
//        }
//
//        mMapView.onSaveInstanceState(mapViewBundle);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mMapView.onResume();
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        mMapView.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        mMapView.onStop();
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//         if (start == "Речной вокзал" && finish == "Мемориал Вечно живым"){
//        PolylineOptions draw_line1 = new PolylineOptions()
//                .add(new LatLng(49.0588389, 33.4028873))
//                .add(new LatLng(49.0589039, 33.4024823))
//                .add(new LatLng(49.0589865, 33.4022677))
//                .add(new LatLng(49.0590814, 33.4020692))
//                .add(new LatLng(49.0590920, 33.4018627))
//                .add(new LatLng(49.0590726, 33.4017205))
//                .add(new LatLng(49.0589320, 33.4016454))
//                .add(new LatLng(49.0588617, 33.4016776))
//                .add(new LatLng(49.0588143, 33.4017339))
//                .add(new LatLng(49.0587774, 33.4018975))
//                .add(new LatLng(49.0587615, 33.4020773))
//                .add(new LatLng(49.0587879, 33.4025654))
//                .add(new LatLng(49.0588389, 33.4029356))
//                .add(new LatLng(49.0589074, 33.4047836))
//                .add(new LatLng(49.0622960, 33.4131414))
//                .add(new LatLng(49.0638179, 33.4169286))
//                .add(new LatLng(49.0660199, 33.4222341))
//                .add(new LatLng(49.0673573, 33.4255305))
//                .add(new LatLng(49.0676296, 33.4261286))
//                .add(new LatLng(49.0678669, 33.4264934))
//                .add(new LatLng(49.0678985, 33.4264451))
//                .add(new LatLng(49.0686682, 33.4245542))
//                .add(new LatLng(49.0692885, 33.4229368))
//                .add(new LatLng(49.0700441, 33.4210190))
//                .add(new LatLng(49.0701214, 33.4208313))
//                .add(new LatLng(49.0701970, 33.4206998))
//                .add(new LatLng(49.0702796, 33.4206167))
//                .add(new LatLng(49.0703780, 33.4205711))
//                .add(new LatLng(49.0711107, 33.4206328))
//                .add(new LatLng(49.0733423, 33.4208098))
//                .add(new LatLng(49.0744018, 33.4208661))
//                .add(new LatLng(49.0746004, 33.4209439))
//                .add(new LatLng(49.0747216, 33.4210297))
//                .add(new LatLng(49.0751486, 33.4215957))
//                .add(new LatLng(49.0766947, 33.4236073))
//                .add(new LatLng(49.0775117, 33.4239453))
//                .add(new LatLng(49.0792440, 33.4247017))
//                .add(new LatLng(49.0803421, 33.4251067))
//                .add(new LatLng(49.0820866, 33.4257585))
//                .add(new LatLng(49.0842456, 33.4266114))
//                .add(new LatLng(49.0858460, 33.4272283))
//                .add(new LatLng(49.0863993, 33.4275314))
//                .add(new LatLng(49.0879030, 33.4281671))
//                .add(new LatLng(49.0887075, 33.4285131))
//                .add(new LatLng(49.0896613, 33.4289101))
//                .add(new LatLng(49.0901952, 33.4291568))
//                .add(new LatLng(49.0907784, 33.4293795))
//                .add(new LatLng(49.0918077, 33.4298059))
//                .add(new LatLng(49.0933287, 33.4304604))
//                .add(new LatLng(49.0943509, 33.4308815))
//                .add(new LatLng(49.0956032, 33.4314287))
//                .add(new LatLng(49.0962969, 33.4317049))
//                .add(new LatLng(49.0975052, 33.4322146))
//                .add(new LatLng(49.0987908, 33.4327671))
//                .add(new LatLng(49.0998902, 33.4332660))
//                .add(new LatLng(49.1009438, 33.4337139))
//                .add(new LatLng(49.1018482, 33.4341189))
//                .add(new LatLng(49.1020800, 33.4341645))
//                .add(new LatLng(49.1023522, 33.4341404))
//                .add(new LatLng(49.1062348, 33.4357685))
//                .add(new LatLng(49.1064788, 33.4360528))
//                .add(new LatLng(49.1065227, 33.4362057))
//                .add(new LatLng(49.1066123, 33.4363988))
//                .add(new LatLng(49.1067264, 33.4365329))
//                .add(new LatLng(49.1068037, 33.4365410))
//                .add(new LatLng(49.1069266, 33.4365115))
//                .add(new LatLng(49.1070109, 33.4364176))
//                .add(new LatLng(49.1071022, 33.4362674))
//                .add(new LatLng(49.1073708, 33.4362137))
//                .add(new LatLng(49.1086860, 33.4367770))
//                .add(new LatLng(49.1092039, 33.4370264))
//                .add(new LatLng(49.1099273, 33.4373590))
//                .add(new LatLng(49.1104365, 33.4375522))
//                .add(new LatLng(49.1128226, 33.4385473))
//                .add(new LatLng(49.1132053, 33.4387055))
//                .add(new LatLng(49.1145730, 33.4392929))
//                .add(new LatLng(49.1154560, 33.4396604))
//                .add(new LatLng(49.1159827, 33.4398857))
//                .add(new LatLng(49.1175575, 33.4405428))
//                .add(new LatLng(49.1197413, 33.4414977))
//                .add(new LatLng(49.1212036, 33.4421173))
//                .add(new LatLng(49.1216231, 33.4425062))
//                .add(new LatLng(49.1217688, 33.4426001))
//                .add(new LatLng(49.1219847, 33.4426993))
//                .add(new LatLng(49.1221901, 33.4427664))
//                .add(new LatLng(49.1224078, 33.4428602))
//                .add(new LatLng(49.1226008, 33.4429032))
//                .add(new LatLng(49.1227606, 33.4429434))
//                .add(new LatLng(49.1229028, 33.4429461))
//                .add(new LatLng(49.1230590, 33.4429166))
//                .add(new LatLng(49.1232907, 33.4428790))
//                .add(new LatLng(49.1235277, 33.4428388))
//                .add(new LatLng(49.1236628, 33.4427959))
//                .add(new LatLng(49.1238524, 33.4426913))
//                .add(new LatLng(49.1239805, 33.4426162))
//                .add(new LatLng(49.1243298, 33.4424579))
//                .add(new LatLng(49.1252022, 33.4423667))
//                .add(new LatLng(49.1265099, 33.4421790))
//                .add(new LatLng(49.1272716, 33.4420797))
//                .add(new LatLng(49.1291970, 33.4418249))
//                .add(new LatLng(49.1311276, 33.4415567))
//                .add(new LatLng(49.1332477, 33.4412670))
//                .add(new LatLng(49.1367996, 33.4407949))
//                .add(new LatLng(49.1385825, 33.4404409))
//                .add(new LatLng(49.1393055, 33.4401941))
//                .add(new LatLng(49.1413059, 33.4390676))
//                .add(new LatLng(49.1420007, 33.4384131))
//                .add(new LatLng(49.1424288, 33.4381020))
//                .add(new LatLng(49.1431026, 33.4374583))
//                .add(new LatLng(49.1435307, 33.4370399))
//                .add(new LatLng(49.1441063, 33.4363854))
//                .add(new LatLng(49.1445484, 33.4359670))
//                .add(new LatLng(49.1455590, 33.4351408))
//                .add(new LatLng(49.1473697, 33.4333706))
//                .add(new LatLng(49.1481838, 33.4325767))
//                .add(new LatLng(49.1493066, 33.4315574))
//                .add(new LatLng(49.1482118, 33.4325820))
//                .add(new LatLng(49.1455345, 33.4351301))
//                .add(new LatLng(49.1445554, 33.4360045))
//                .add(new LatLng(49.1441168, 33.4363854))
//                .add(new LatLng(49.1430710, 33.4374475))
//                .add(new LatLng(49.1412988, 33.4390891))
//                .add(new LatLng(49.1402741, 33.4397542))
//                .add(new LatLng(49.1392914, 33.4402156))
//                .add(new LatLng(49.1385544, 33.4404516))
//                .add(new LatLng(49.1367856, 33.4407628))
//                .add(new LatLng(49.1349815, 33.4409881))
//                .add(new LatLng(49.1311416, 33.4415567))
//                .add(new LatLng(49.1272523, 33.4420931))
//                .add(new LatLng(49.1242684, 33.4424472))
//                .add(new LatLng(49.1238401, 33.4423614))
//                .add(new LatLng(49.1232363, 33.4424150))
//                .add(new LatLng(49.1228992, 33.4424257))
//                .add(new LatLng(49.1226324, 33.4424257))
//                .add(new LatLng(49.1222392, 33.4423077))
//                .add(new LatLng(49.1219443, 33.4422004))
//                .add(new LatLng(49.1215933, 33.4421253))
//                .add(new LatLng(49.1211649, 33.4421146))
//                .add(new LatLng(49.1196904, 33.4414923))
//                .add(new LatLng(49.1151190, 33.4395075))
//                .add(new LatLng(49.1104839, 33.4375763))
//                .add(new LatLng(49.1073515, 33.4362137))
//                .add(new LatLng(49.1071549, 33.4359562))
//                .add(new LatLng(49.1070916, 33.4357417))
//                .add(new LatLng(49.1070003, 33.4355485))
//                .add(new LatLng(49.1068669, 33.4354949))
//                .add(new LatLng(49.1067264, 33.4355700))
//                .add(new LatLng(49.1065508, 33.4356773))
//                .add(new LatLng(49.1063963, 33.4357095))
//                .add(new LatLng(49.1061786, 33.4357417))
//                .add(new LatLng(49.1023364, 33.4341216))
//                .add(new LatLng(49.1019431, 33.4337354))
//                .add(new LatLng(49.1012266, 33.4334457))
//                .add(new LatLng(49.0961547, 33.4312892))
//                .add(new LatLng(49.0908012, 33.4290683))
//                .add(new LatLng(49.0891571, 33.4283173))
//                .add(new LatLng(49.0864344, 33.4271908))
//                .add(new LatLng(49.0854542, 33.4268904))
//                .add(new LatLng(49.0842808, 33.4264827))
//                .add(new LatLng(49.0819900, 33.4256136))
//                .add(new LatLng(49.0790525, 33.4244549))
//                .add(new LatLng(49.0775416, 33.4238112))
//                .add(new LatLng(49.0765225, 33.4233767))
//                .color(Color.GRAY).width(15);
//        //line2 = mMap.addPolyline(draw_line1);
//        //чтобы убрать : line2.remove();
//    }

      //else if (start == "Киевская" && finish == "Железнодорожный вокзал") {
// строим следущий полилайн
//      }
//    @Override
//    protected void onPause() {
//        mMapView.onPause();
//        super.onPause();
//    }
//
//    @Override
//    protected void onDestroy() {
//        mMapView.onDestroy();
//        super.onDestroy();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mMapView.onLowMemory();
//    }
}
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });




