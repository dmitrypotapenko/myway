package com.dima.myway.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dima.myway.Problem;
import com.dima.myway.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentProblem.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentProblem#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentProblem extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Button Send, Mygps;

    EditText name, Sname, number, probl;
    TextView GPS;
    String forgps = "";
    Problem problem;
    DatabaseReference myRef;
    LocationManager locationManager;

    //    OnFragmentInteractionListener mCallback;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private OnFragmentInteractionListener mListener;
    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled();
        }

        @Override
        public void onProviderEnabled(String provider) {
            checkEnabled();
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            showLocation(locationManager.getLastKnownLocation(provider));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                //tvStatusGPS.setText("Status: " + String.valueOf(status));
            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                // tvStatusNet.setText("Status: " + String.valueOf(status));
            }
        }
    };

    public FragmentProblem() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentProblem.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentProblem newInstance(String param1, String param2) {
        FragmentProblem fragment = new FragmentProblem();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_problem, container, false);

        Button button = (Button) rootView.findViewById(R.id.MyGPS);
        Button button2 = (Button) rootView.findViewById(R.id.Send);

        Send = (Button) rootView.findViewById(R.id.Send);
        Mygps = (Button) rootView.findViewById(R.id.MyGPS);

        name = (EditText) rootView.findViewById(R.id.name);
        Sname = (EditText) rootView.findViewById(R.id.Sname);
        number = (EditText) rootView.findViewById(R.id.Phone);
        probl = (EditText) rootView.findViewById(R.id.Problem);

        GPS = (TextView) rootView.findViewById(R.id.gps);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        //locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        Send.setOnClickListener(this);
        Mygps.setOnClickListener(this);

        button.setOnClickListener(this);
        button2.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000 * 10, 10, locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 1000 * 10, 10,
                locationListener);
        checkEnabled();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(locationListener);
    }

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            forgps = formatLocation(location);
            //GPS.setText(formatLocation(location));
        } else if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
            forgps = formatLocation(location);
            //GPS.setText(formatLocation(location));
        }
    }

    private String formatLocation(Location location) {
        if (location == null)
            return "";
        return String.format(
                "%1$.4f, %2$.4f",
                location.getLatitude(), location.getLongitude());
    }

    private void checkEnabled() {
        locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.Send:
                problem = new Problem(name.getText().toString(), Sname.getText().toString(), number.getText().toString(), probl.getText().toString(),
                        GPS.getText().toString());

                if (name.getText().toString().equals("") || Sname.getText().toString().equals("") || number.getText().toString().equals("") || probl.getText().toString().equals("")
                        || GPS.getText().toString().equals(""))
                {
                    Toast.makeText(getContext(), "Заполните все данные", Toast.LENGTH_LONG).show();
                    break;
                }
                else
                {   // Write a message to the database
                    myRef.push().setValue(problem);

                    Toast.makeText(getContext(), "Отправлено!", Toast.LENGTH_LONG).show();

                    name.getText().clear();
                    Sname.getText().clear();
                    number.getText().clear();
                    probl.getText().clear();
                    GPS.setText("");
                    break;
                }
            case R.id.MyGPS:
                GPS.setText(forgps);
                break;
            default:
                break;
        }
    }
}


