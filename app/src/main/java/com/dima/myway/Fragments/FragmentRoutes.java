package com.dima.myway.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dima.myway.DatabaseHelper;
import com.dima.myway.Item.Route;
import com.dima.myway.OnTapListener;
import com.dima.myway.R;
import com.dima.myway.RouteInfo;
import com.dima.myway.adapter.FirstAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentRoutes.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentRoutes#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRoutes extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    Button button3, button4;
    private String tableName;
    private RecyclerView recyclerView;
    private DatabaseHelper databaseHelper;
    private ArrayList<Route> arrayList = new ArrayList<Route>();
    private Cursor cursor;
    private FirstAdapter adapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
//    private OnFragmentInteractionListener mListener;

    public FragmentRoutes() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentRoutes.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRoutes newInstance(String param1, String param2) {
        FragmentRoutes fragment = new FragmentRoutes();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_routes, container, false);
        button3 = (Button) viewGroup.findViewById(R.id.button_marshrutka);
        button4 = (Button) viewGroup.findViewById(R.id.button_trolley);

        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.recycler_view);
        initList();
        loadRoutes(RouteType.MINI_BUS);
        return viewGroup;
    }


    public void initList() {
        arrayList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        adapter = new FirstAdapter(getActivity(), arrayList);
        adapter.setOnTapListener(new OnTapListener() {
            @Override
            public void OnTapView(int position, Route route) {
                Intent intent = new Intent(getActivity(), RouteInfo.class);
                intent.putExtra("id", route.getId());
                intent.putExtra("tableNamed", tableName);
                startActivity(intent);
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_marshrutka:
                loadRoutes(RouteType.MINI_BUS);
                break;
            case R.id.button_trolley:
                loadRoutes(RouteType.TROLLEY);
                break;
            default:
                break;
        }
    }

    private void loadRoutes(RouteType routeType) {
        if (arrayList == null) {
            arrayList = new ArrayList<>();
        }
        arrayList.clear();
        databaseHelper = new DatabaseHelper(getActivity());
        try {
            databaseHelper.checkAndCopyDatabase();
            databaseHelper.openDatabase();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }


        tableName = routeType == RouteType.MINI_BUS ? "ways_minibus" : "ways_trolleybus";
        try {
            cursor = databaseHelper.QueryData("select * from " + tableName); //это вкинуть по нажатию на кнопку имя таблицы менять
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        Route route = new Route(cursor.getInt(0), cursor.getString(1));
                        arrayList.add(route);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        adapter.setRoutes(arrayList);
        adapter.notifyDataSetChanged();

    }
}
