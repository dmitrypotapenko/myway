package com.dima.myway.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.dima.myway.Actmap;
import com.dima.myway.Optways;
import com.dima.myway.R;


public class FragmentDirections extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "adress";
    private static final String ARG_PARAM4 = "adress2";

    ImageButton search, poisk, change;
    EditText departure, dest;
    Button marshrut, type_transport;
    String first, second, transp, put, Change;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int index4, index5;

    public FragmentDirections() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDirections.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDirections newInstance(String param1, String param2) {
        FragmentDirections fragment = new FragmentDirections();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            first = getArguments().getString(ARG_PARAM3);   //конец
            second = getArguments().getString(ARG_PARAM4); //начало
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView1 = inflater.inflate(R.layout.fragment_directions, container, false);

        search = (ImageButton) rootView1.findViewById(R.id.search);
        poisk = (ImageButton) rootView1.findViewById(R.id.poisk);
        change = (ImageButton) rootView1.findViewById(R.id.change);
        departure = (EditText) rootView1.findViewById(R.id.departure);
        dest = (EditText) rootView1.findViewById(R.id.dest);
        marshrut = (Button) rootView1.findViewById(R.id.marshrut);
        type_transport = (Button) rootView1.findViewById(R.id.type_transport);

        departure.setText(first);
        dest.setText(second);

        search.setOnClickListener(this);
        poisk.setOnClickListener(this);
        change.setOnClickListener(this);

        marshrut.setOnClickListener(this);
        type_transport.setOnClickListener(this);

        return rootView1;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search:
                Intent intent = new Intent(getActivity(), Actmap.class);
                startActivity(intent);
                break;

            case R.id.change:
                Change = departure.getText().toString();
                departure.setText(dest.getText().toString());
                dest.setText(Change);
                break;

            case R.id.poisk:
                Intent intent1 = new Intent(getActivity(), Optways.class);
                intent1.putExtra("start", first);
                intent1.putExtra("finish", second);
                intent1.putExtra("transp", transp);
                intent1.putExtra("put", put);
                startActivity(intent1);
                //поиск оптимальных
                break;
            case R.id.type_transport:
                final String[] mChooseTransport = {"Любой", "Троллейбус", "Маршрутка"};
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setTitle("Выберите тип транспорта")
                        .setCancelable(false)

                        // добавляем переключатели
                        .setSingleChoiceItems(mChooseTransport, index4,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        index4 = item;
                                        transp = mChooseTransport[index4].toString();
                                    }
                                })

                        .setPositiveButton("OК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });

                AlertDialog alert1 = builder1.create();
                alert1.show();
                break;

            case R.id.marshrut:
                final String[] mChooseRoute = {"Любой", "Быстрый", "Дешевый"};
                builder1 = new AlertDialog.Builder(getActivity());
                builder1.setTitle("Выберите тип маршрута")
                        .setCancelable(false)

                        // добавляем переключатели
                        .setSingleChoiceItems(mChooseRoute, index5,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        index5 = item;
                                        put = mChooseRoute[index5].toString();
                                    }
                                })

                        .setPositiveButton("OК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });

                alert1 = builder1.create();
                alert1.show();
            default:
                break;
        }
    }
}
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */


