package com.dima.myway.Fragments;

/**
 * myway
 * Created by Rostyslav Yeromchenko on 17.05.17
 */
public enum RouteType {
    MINI_BUS, TROLLEY
}