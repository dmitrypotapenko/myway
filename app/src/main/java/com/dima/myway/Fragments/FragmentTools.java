package com.dima.myway.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dima.myway.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTools.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTools#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTools extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public int index, index1, index2, index3 = -1;
    Button city, way_download, interval, map;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentTools() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentTools newInstance(String param1, String param2) {
        FragmentTools fragment = new FragmentTools();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView7 = inflater.inflate(R.layout.fragment_tools, container, false);
        city = (Button) rootView7.findViewById(R.id.city);
        way_download = (Button) rootView7.findViewById(R.id.way_download);
        interval = (Button) rootView7.findViewById(R.id.interval);
        map = (Button) rootView7.findViewById(R.id.map);
        city.setOnClickListener(this);
        way_download.setOnClickListener(this);
        interval.setOnClickListener(this);
        map.setOnClickListener(this);
        return rootView7;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.city):
                final String[] mChooseCity = {"Кременчуг", "Комсомольск", "Полтава"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle("Выберите город")
                        .setCancelable(false)

                        // добавляем переключатели
                        .setSingleChoiceItems(mChooseCity, index,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        index = item;
                                    }
                                })

                        .setPositiveButton("OК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });

                AlertDialog alert = builder.create();
                alert.show();
                break;

            case (R.id.way_download):
                final String[] mChooseWaydownload = {"Любой", "Wi-Fi", "Мобильный интернет"};
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Выберите способ загрузки данных")
                        .setCancelable(false)

                        // добавляем переключатели
                        .setSingleChoiceItems(mChooseWaydownload, index1,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        index1 = item;
                                    }
                                })

                        .setPositiveButton("OК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });

                alert = builder.create();
                alert.show();

                break;
            case (R.id.interval):
                final String[] mChooseInterval = {"15 сек", "30 сек", "1 мин"};
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Выберите интервал обновления GPS")
                        .setCancelable(false)

                        // добавляем переключатели
                        .setSingleChoiceItems(mChooseInterval, index2,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        index2 = item;
                                    }
                                })

                        .setPositiveButton("OК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });

                alert = builder.create();
                alert.show();
                break;
            case R.id.map:
                final String[] mChooseMap = {"Карта", "Спутник", "Гибрид"};
                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Выберите способ загрузки данных")
                        .setCancelable(false)

                        // добавляем переключатели
                        .setSingleChoiceItems(mChooseMap, index3,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        index3 = item;
                                    }
                                })

                        .setPositiveButton("OК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                });

                alert = builder.create();
                alert.show();
                break;
            default:
        }
    }
}
