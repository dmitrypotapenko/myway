package com.dima.myway.Item;

/**
 * Created by Dima on 16.05.2017.
 */

public class Route {
    int id;
    String name;

    public Route(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String get_name() {
        return name;
    }

    public void set_name(String _name) {
        this.name = _name;
    }
}
