package com.dima.myway;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class RouteInfo extends AppCompatActivity {

    private int id;
    private String tableNamed;
    private TextView common;
    private DatabaseHelper databaseHelper;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        databaseHelper = new DatabaseHelper(this);
        try {
            databaseHelper.checkAndCopyDatabase();
            databaseHelper.openDatabase();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

        Intent intent = getIntent();
        id = intent.getIntExtra("id", id);
        tableNamed = intent.getStringExtra("tableNamed");

        common = (TextView) findViewById(R.id.common);

        //name.setText(Integer.toString(id));   ROUTE_ID"_id"

        try {
            cursor = databaseHelper.QueryData("select * from " + tableNamed + " where " + "_id" + "= " + id); //это вкинуть по нажатию на кнопку имя таблицы менять
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        common.setText("Название " + (cursor.getString(2).toString())
                                + "\n\n" + "Начало " + (cursor.getString(3))
                                + "\n\n" + "Конец " + (cursor.getString(4))
                                + "\n\n" + "Цена " + (cursor.getString(5))
                                + "\n\n" + "Расстояние " + (cursor.getString(6))
                                + "\n\n" + "Интервал " + (cursor.getString(7))
                                + "\n\n" + "Часы работы " + (cursor.getString(8))
                                + "\n\n" + "Перевозчик " + (cursor.getString(9))
                                + "\n\n" + "Телефон " + (cursor.getString(10)));

                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
