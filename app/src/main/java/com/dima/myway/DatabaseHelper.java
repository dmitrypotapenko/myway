package com.dima.myway;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Dima on 16.05.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String ROUTE_ID = "_id";
    public static final String ROUTE_NAME = "name";
    public static final String ROUTE_BEGIN = "begin";
    public static final String ROUTE_END = "end";
    public static final String ROUTE_PRICE = "price";
    public static final String ROUTE_DISTANCE = "distance";
    public static final String ROUTE_INTERVAL = "interval";
    public static final String ROUTE_OPENHOURS = "open_hours";
    public static final String ROUTE_CARRIER = "carrier";
    public static final String ROUTE_TELEPHONE = "telephone";
    private static String DB_NAME="StopsDB.db";
    private static String DB_PATH = "/data/data/com.dima.myway/databases/";

    private SQLiteDatabase myDatabase;
    private Context myContext;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
//        if (Build.VERSION.SDK_INT>=15) {
//            DB_PATH=context.getApplicationInfo().dataDir + "/databases/";
//        } else {
//            DB_PATH= Environment.getDataDirectory()+"/data/"+context.getPackageName()+"/databases/";
//        }
        this.myContext=context;
    }

//    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
//        super(context, name, factory, version, errorHandler);
//    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void checkAndCopyDatabase()
    {
        boolean dbExist= checkDatabase();
        if (dbExist){
            Log.d("TAG", "database already exist");
        } else {
            this.getReadableDatabase();
        }
        try {
            copyDatabase();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", "Error copy database");
        }
    }

    public boolean checkDatabase() {
        SQLiteDatabase checkDB=null;
        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException e) {}
            if (checkDB !=null) {
            checkDB.close();
        }
        return checkDB !=null ? true : false;
    }

    public void copyDatabase() throws IOException{
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH +DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length=myInput.read(buffer))>0) {
            myOutput.write(buffer, 0 , length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDatabase(){
        String myPath = DB_PATH+DB_NAME;
        myDatabase= SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READWRITE);
    }

    public synchronized void close(){
        if(myDatabase !=null) {
            myDatabase.close();
        }
        super.close();
    }

    public Cursor QueryData(String query) {
        return myDatabase.rawQuery(query,null);
    }
}
