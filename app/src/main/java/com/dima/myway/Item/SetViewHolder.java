package com.dima.myway.Item;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.dima.myway.R;


/**
 * Created by Dima on 16.05.2017.
 */

public class SetViewHolder extends RecyclerView.ViewHolder{
    public TextView txt_name;
    public SetViewHolder(View itemView) {
        super(itemView);
        txt_name = (TextView) itemView.findViewById(R.id.tv_route_name);
        //txt_name = (TextView) itemView.findViewById(R.id.text_word);
    }
}
