package com.dima.myway;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.dima.myway.Fragments.FragmentDirections;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Actmap extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnInfoWindowClickListener {

    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    private static final String ARG_PARAM3 = "adress";
    private static final String ARG_PARAM4 = "adress2";
    String markertitle;
    ImageButton check;
    MapView mMapView;
    GoogleMap mMap;
    EditText beginer,end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actmap);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(mapViewBundle);
        mMapView.getMapAsync(this);

        beginer = (EditText) findViewById(R.id.beginer);
        end = (EditText) findViewById(R.id.end);

        check = (ImageButton) findViewById(R.id.check);
        check.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mMapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnInfoWindowClickListener(this);

        LatLng kolesny_zavod = new LatLng(49.111237,33.412664);
        mMap.addMarker(new MarkerOptions().position(kolesny_zavod).title("Колесный завод"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(kolesny_zavod,14));

        LatLng PO_AutoKraz = new LatLng(49.105816,33.416497);
        mMap.addMarker(new MarkerOptions().position(PO_AutoKraz).title("ПО АвтоКраз"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(PO_AutoKraz,14));

        LatLng kievskaya = new LatLng(49.104152,33.412795);
        mMap.addMarker(new MarkerOptions().position(kievskaya).title("Киевская"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(kievskaya,14));

        LatLng KTU = new LatLng(49.102234,33.415103);
        mMap.addMarker(new MarkerOptions().position(KTU).title("КТУ"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(KTU,14));

        LatLng AutoPark = new LatLng(49.098622,33.419386);
        mMap.addMarker(new MarkerOptions().position(AutoPark).title("Автопарк"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(AutoPark,14));

        LatLng Novoivanovka = new LatLng(49.094445,33.423261);
        mMap.addMarker(new MarkerOptions().position(Novoivanovka).title("м/н Новоивановка"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Novoivanovka,14));

        LatLng Memorial = new LatLng(49.089031,33.428507);
        mMap.addMarker(new MarkerOptions().position(Memorial).title("Мемориал Вечно живым"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Memorial,14));

        LatLng Vodokanal = new LatLng(49.084578,33.426677);
        mMap.addMarker(new MarkerOptions().position(Vodokanal).title("Водоканал"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Vodokanal,14));

        LatLng Troitskyy = new LatLng(49.080444,33.424795);
        mMap.addMarker(new MarkerOptions().position(Troitskyy).title("Троицкий рынок"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Troitskyy,14));

        LatLng Gorsad = new LatLng(49.078169,33.424070);
        mMap.addMarker(new MarkerOptions().position(Gorsad).title("Городской сад"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Gorsad,14));

        LatLng Kredmash = new LatLng(49.073880,33.420834);
        mMap.addMarker(new MarkerOptions().position(Kredmash).title("Кредмаш"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Kredmash,14));

        LatLng Halamenyuka = new LatLng(49.070389,33.420606);
        mMap.addMarker(new MarkerOptions().position(Halamenyuka).title("Халаменюка"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Halamenyuka,14));

        LatLng ZD = new LatLng(49.068346,33.425515);
        mMap.addMarker(new MarkerOptions().position(ZD).title("Железнодорожный вокзал"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ZD,14));

        LatLng Rynok = new LatLng(49.064154,33.417718);
        mMap.addMarker(new MarkerOptions().position(Rynok).title("Рынок"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Rynok,14));

        LatLng GDK = new LatLng(49.062458,33.413484);
        mMap.addMarker(new MarkerOptions().position(GDK).title("ГДК им.Петровского"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(GDK,14));

        LatLng Pobedy = new LatLng(49.060207,33.408050);
        mMap.addMarker(new MarkerOptions().position(Pobedy).title("Победы"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Pobedy,14));

        LatLng Rechnoy_vokzal = new LatLng(49.058817,33.402534);
        mMap.addMarker(new MarkerOptions().position(Rechnoy_vokzal).title("Речной вокзал"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Rechnoy_vokzal,14));

        LatLng GAI = new LatLng(99.999999,99.999999);
        mMap.addMarker(new MarkerOptions().position(GAI).title("ГАИ"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(GAI,14));

        LatLng VadimaBoyko = new LatLng(49.093267,33.430251);
        mMap.addMarker(new MarkerOptions().position(VadimaBoyko).title("Вадима Бойко"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(VadimaBoyko,14));

        LatLng Gvardeyskaya = new LatLng(49.097611,33.432119);
        mMap.addMarker(new MarkerOptions().position(Gvardeyskaya).title("Гвардейская"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Gvardeyskaya,14));

        LatLng Moskovskaya = new LatLng(49.101116,33.433590);
        mMap.addMarker(new MarkerOptions().position(Moskovskaya).title("Московская"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Moskovskaya,14));

        LatLng Pivzavod = new LatLng(49.107912,33.436450);
        mMap.addMarker(new MarkerOptions().position(Pivzavod).title("Пивзавод"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Pivzavod,14));

        LatLng Kovaleva = new LatLng(49.111624,33.438025);
        mMap.addMarker(new MarkerOptions().position(Kovaleva).title("Ковалёва"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Kovaleva,14));

        LatLng SovArmii = new LatLng(49.116562,33.440142);
        mMap.addMarker(new MarkerOptions().position(SovArmii).title("Советской Армии"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(SovArmii,16));

        LatLng Zhukova = new LatLng(49.122783,33.442696);
        mMap.addMarker(new MarkerOptions().position(Zhukova).title("Маршала Жукова"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Zhukova,14));

        LatLng Kerchenskaya = new LatLng(49.128634,33.441898);
        mMap.addMarker(new MarkerOptions().position(Kerchenskaya).title("Керченская"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Kerchenskaya,14));

        LatLng GeroevStalingrada = new LatLng(49.133551,33.441210);
        mMap.addMarker(new MarkerOptions().position(GeroevStalingrada).title("Героев Сталинграда"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(GeroevStalingrada,14));

        LatLng PTU = new LatLng(49.139142,33.440223);
        mMap.addMarker(new MarkerOptions().position(PTU).title("ПТУ"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(PTU,14));

        LatLng KinoteatrAvrora = new LatLng(49.143716,33.436802);
        mMap.addMarker(new MarkerOptions().position(KinoteatrAvrora).title("Кинотеатр Аврора"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(KinoteatrAvrora,14));

        LatLng Molodezhnaya = new LatLng(49.148777,33.431924);
        mMap.addMarker(new MarkerOptions().position(Molodezhnaya).title("Молодежная"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Molodezhnaya,14));

        LatLng Kladbishe = new LatLng(49.161628,33.436633);
        mMap.addMarker(new MarkerOptions().position(Kladbishe).title("Кладбище"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Kladbishe,14));

        LatLng KZTU = new LatLng(99.999999,99.999999);
        mMap.addMarker(new MarkerOptions().position(KZTU).title("КЗТУ"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(KZTU,14));

        LatLng Trest = new LatLng(99.999999,99.999999);
        mMap.addMarker(new MarkerOptions().position(Trest).title("Трест"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Trest,14));

        LatLng TEC = new LatLng(99.999999,99.999999);
        mMap.addMarker(new MarkerOptions().position(TEC).title("ТЭЦ"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(TEC,16));

        LatLng KNPZ = new LatLng(49.171573,33.453347);
        mMap.addMarker(new MarkerOptions().position(KNPZ).title("КНПЗ"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(KNPZ,14));

        LatLng Krukovskyymist = new LatLng(49.062297,33.418011);
        mMap.addMarker(new MarkerOptions().position(Krukovskyymist).title("Крюковский мост"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Krukovskyymist,14));

        LatLng Centr = new LatLng(49.068513,33.412105);
        mMap.addMarker(new MarkerOptions().position(Centr).title("Центр"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Centr,14));

        LatLng Krasina = new LatLng(49.071120,33.409455);
        mMap.addMarker(new MarkerOptions().position(Krasina).title("Красина"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Krasina,14));

        LatLng Stanciya = new LatLng(49.073659,33.413916);
        mMap.addMarker(new MarkerOptions().position(Stanciya).title("Станция Юных Техников"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Stanciya,14));

        LatLng Geroev_Chernobylya = new LatLng(49.075459,33.417976);
        mMap.addMarker(new MarkerOptions().position(Geroev_Chernobylya).title("Героев Чернобыля"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Geroev_Chernobylya,14));
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.check){
            FragmentDirections fragobj = new FragmentDirections();
            Bundle args=new Bundle();
            args.putString(ARG_PARAM3, beginer.getText().toString());
            args.putString(ARG_PARAM4, end.getText().toString());
            fragobj.setArguments(args);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_actmap, fragobj);
            //transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commit();
            //Открыть фрагмент и передать туда как параметр адрес
        }

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        markertitle = marker.getTitle();
        if (beginer.getText().toString().isEmpty()) {
            beginer.setText(markertitle);
        } else {
            end.setText(markertitle);
        }

    }
}
