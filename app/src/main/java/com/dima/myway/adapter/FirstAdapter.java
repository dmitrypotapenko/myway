package com.dima.myway.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dima.myway.Item.Route;
import com.dima.myway.Item.SetViewHolder;
import com.dima.myway.OnTapListener;
import com.dima.myway.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dima on 16.05.2017.
 */

public class FirstAdapter extends RecyclerView.Adapter<SetViewHolder> {
    List<Route> mRoutes = Collections.emptyList();
    private Activity activity;
    private OnTapListener onTapListener;

    public FirstAdapter(Activity activity, List<Route> routes) {
        this.activity = activity;
        this.mRoutes = routes;
    }

    public void setRoutes(List<Route> routes) {
        this.mRoutes = routes;
    }

    @Override
    public SetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new SetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SetViewHolder holder, final int position) {
        final Route route = mRoutes.get(position);
        holder.txt_name.setText(route.get_name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTapListener != null) {
                    onTapListener.OnTapView(position, route);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mRoutes.size();
    }

    public void setOnTapListener(OnTapListener onTapListener) {
        this.onTapListener = onTapListener;
    }
}
