package com.dima.myway;

import com.dima.myway.Item.Route;

/**
 * Created by Dima on 16.05.2017.
 */

public interface OnTapListener {
    public void OnTapView(int position, Route route);
}
