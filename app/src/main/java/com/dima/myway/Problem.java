package com.dima.myway;

public class Problem {
    private String name;
    private String Sname;
    private String number;
    private String problem;
    private String gps;

    public Problem(String name, String Sname, String number, String problem, String gps) {
        this.name = name;
        this.Sname = Sname;
        this.number = number;
        this.problem = problem;
        this.gps = gps;
    }

    public String getName() {
        return name;
    }

    public String getSName() {
        return Sname;
    }

    public String getNumber() {
        return number;
    }

    public String getProblem() {
        return problem;
    }

    public String getGps() {
        return gps;
    }
}
